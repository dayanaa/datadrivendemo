package tests;

import java.io.File;
import java.io.IOException;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;
import pages.ErrorPage;
import pages.LoginPage;
import utility.DriverManagerBase;
import utility.ExcelUtils;

public class LoginTest {

	WebDriver driver;
	ExcelUtils excel;

	@BeforeMethod
	public void setUp() {
		driver=DriverManagerBase.driverInitialize();
	}

	// @Parameters({"username","incorrectpassword"})
	@Test(description = "Performs an Unsuccessful login and checks the resulting error message")
	public void testLoginNOK() {

		LoginPage lp = new LoginPage(driver);
		ErrorPage ep = lp.incorrectLogin("admin", "admin");

		Assert.assertEquals(ep.getErrorText(), "The verified.");
		// Assert.assertEquals(ep.getErrorText(), "The username and password could not
		// be verified.");
	}

	// @Parameters({"username","password"})
	@Test(description = "Performs an successful login", dataProvider = "TestData", enabled = false)
	public void testLoginOK(String username, String password) {

		LoginPage lp = new LoginPage(driver);
		lp.correctLogin(username, password);
	}

//	@DataProvider(name="TestData")
//	public Object[][] testData(){
//		String usrname="admin";
//		String passwrd="admin";
//		return new Object[][] {{"admin","admin"},{"hgfv","ggf"}};
//	}
//	

	@DataProvider(name = "TestData")
	public Object[][] testData() throws Exception {
		excel = new ExcelUtils();
		excel.excelDataConfig("src/test/resources/testData/LoginUserTestData.xlsx", "Sheet1");
		String usrname = excel.getCellData("Username", 1);
		String passwrd = excel.getCellData("Password", 1);
		return new Object[][] { { usrname, passwrd } };
	}

	@AfterMethod
	public void tearDown() {

		DriverManagerBase.quit();
		
	}

}
