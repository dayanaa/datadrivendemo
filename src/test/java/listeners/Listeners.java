package listeners;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.Markup;

import tests.LoginTest;
import utility.DriverManagerBase;
import utility.ExtentReportNG;

public class Listeners extends DriverManagerBase implements ITestListener  {
	ExtentReports extent;
	ExtentTest test;
	WebDriver driver; 
	
	
	

	@Override
	public void onTestStart(ITestResult result) {
		// before each Test case
		Reporter.log("Method name is : "+result.getName());
		System.out.println("Test Starting");
		
		test=extent.createTest(result.getMethod().getMethodName());
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		//Reporter.log("Status of Exceution is : "+result.getStatus());
		
		test.log(Status.PASS, "Testcase"+result.getMethod().getMethodName()+" is passed");
		
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		test.log(Status.FAIL, "Testcase"+result.getMethod().getMethodName()+" is failed");
		test.log(Status.FAIL, result.getThrowable());
		driver= DriverManagerBase.driver;
		
		File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy HH-mm-ssZ");
		Date date=new Date();
		System.out.println(date);
		String actualDate=format.format(date);
		String path=System.getProperty("user.dir")+"/ExtentReports/Screenshots/"+actualDate+".png";
//		String path="ExtentReports/Screenshots/"+actualDate+".png";
		System.out.print(path);
		File des=new File(path);
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		test.fail("Screenshot :"+test.addScreenCaptureFromPath(path,"Failed Page Screenshot"));

	}
		

	@Override
	public void onTestSkipped(ITestResult result) {
		test.log(Status.SKIP, "Testcase"+result.getMethod().getMethodName()+" is skipped");
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		extent=ExtentReportNG.setupExtentReport();
		System.out.println("onstart of the suite");
		
		
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
		extent.flush();
		
	}

}
