package pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import utility.DriverManagerBase;

public class LoginPage extends DriverManagerBase {
	//page Factory
	@FindBy(how=How.NAME,using="username")
	private WebElement username;
	
	//By Class
	public static By password = By.name("password");
	
	public static By loginbutton= By.xpath("//input[@value='Log In']");
	
WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		driver.get("http://parabank.parasoft.com");
				
	}
	
	public ErrorPage incorrectLogin(String name, String pwd) {
		
		username.sendKeys(name);
		driver.findElement(password).sendKeys(pwd);
		
		driver.findElement(loginbutton).click();
		return new ErrorPage(driver);
	}
	
	public void correctLogin(String name, String pwd) {
		
		username.sendKeys(name);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(By.xpath("//input[@value='Log In']")).click();
		
	}
}


