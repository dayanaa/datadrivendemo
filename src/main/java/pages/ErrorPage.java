package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utility.DriverManagerBase;

public class ErrorPage extends DriverManagerBase{
	
private WebDriver driver;
	
	

	public ErrorPage(WebDriver driver) {
	// TODO Auto-generated constructor stub
		this.driver=driver;
}



	public String getErrorText() {
		
		return driver.findElement(By.className("error")).getText();
	}
}
