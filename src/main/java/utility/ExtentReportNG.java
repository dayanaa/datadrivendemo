package utility;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentReporter;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentReportNG {
	
	static ExtentReports extentReport;
	
	public static ExtentReports setupExtentReport()
	{
		SimpleDateFormat format=new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
		Date date=new Date();
		String actualDate=format.format(date);
		
		extentReport=new ExtentReports();
		ExtentSparkReporter sparkReporter=new ExtentSparkReporter("ExtentReports/ExecutionReport_"+actualDate+".html");
		extentReport.attachReporter(sparkReporter);
		
		sparkReporter.config().setDocumentTitle("Extent Report Demo");
		sparkReporter.config().setTheme(Theme.DARK);
		sparkReporter.config().setReportName("Test Report");
		
		
		
		return extentReport;
		
	}
	

}
