package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	private FileInputStream fip;
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	//String Excelpath = "src/test/resources/testData/LoginUserTestData.xlsx";
	private Map<String,Integer> columns=new HashMap<>();
	private Cell cell;

	public XSSFSheet excelDataConfig(String Excelpath, String SheetName) {

		try {
		File file = new File(Excelpath);
		fip = new FileInputStream(file);
		//for .xlsx
		workbook = new XSSFWorkbook(fip);
		// for .xls file
		// HSSFWorkbook workbook=new HSSFWorkbook(fip);
		sheet=workbook.getSheet(SheetName);
		
		
		sheet.getRow(0).forEach(cell ->{
			columns.put(cell.getStringCellValue(),cell.getColumnIndex());
		});
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return sheet ;
	}

	public String getCellDatabyType(int rowno,int colno) {
		
		cell=sheet.getRow(rowno).getCell(colno);
		String cellData=null;
		switch(cell.getCellType()) {
		case STRING:
			cellData=cell.getStringCellValue();
			break;
		case NUMERIC:
			cellData=String.valueOf(cell.getNumericCellValue());
			break;
		case BOOLEAN:
			cellData=Boolean.toString(cell.getBooleanCellValue());
			break;
		default:
			cellData="";
			break;
		}
		
		return cellData;
		
	}
	
		
	public String getCellData(String columnName, int rownum) throws Exception {
	    return getCellDatabyType(rownum, columns.get(columnName));
	    
	    
	    
	}
	
	public void closeExcel() throws IOException {
		fip.close();
	}
}

//	public static Object[][] readExcel(String Excelpath,String SheetName)
//	{
//		String[][] values=null;
//		excelDataConfig(Excelpath,SheetName);
//		int totalRows=sheet.getLastRowNum();
//		int totalcolumn=sheet.getRow(0).getLastCellNum();
//		values=new String[totalRows][totalcolumn];
//		for(i=1;i<)
//	}
